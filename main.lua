--Space Rocks - a retro side-scrolling gravity game
--Copyright 2018 Eric Duhamel

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <https://www.gnu.org/licenses/>.

function love.load()
    winwidth, winheight = love.graphics.getDimensions()
    uberW = 800
    uberH = 360
    width = 320
    height = 240
    left = 20
    top = (winheight-height)/2
    right = left+width
    bottom = top+height
    surface = bottom-10
    gravity = 250
    thrust = 600
    speed = 60
    gap = 60
    numThings = (width+gap)/gap
    hiscore = 0
    shipI = love.graphics.newImage("ship.png")
    coinI = love.graphics.newImage("coin.png")
    rockI = love.graphics.newImage("rock.png")
    rocketI = love.graphics.newImage("rocket.png")
    shipS = {16, 16}
    coinS = {10, 10}
    rockS = {50, 50}
    thrustSnd = love.audio.newSource("thrust.wav", "static")
    coinSnd = love.audio.newSource("coin.wav", "static")
    missSnd = love.audio.newSource("miss.wav", "static")
    deathSnd = love.audio.newSource("death.wav", "static")
    loop0 = love.audio.newSource("loop03.mp3", "stream")
    loop1 = love.audio.newSource("loop04.mp3", "stream")
    loop2 = love.audio.newSource("loop05.mp3", "stream")
    loop = loop0
    intro = true
    love.graphics.setNewFont(16)
    gameStart()
end

function gameStart()
    score = 0
    objects = {}
    rocks = {}
    coins = {}
    addRocks()
    addCoins()
    addShip()
    started = 0
end

function addShip()
    deltas = {0,0}
    ship = newObj(left+(width/5),top-shipS[1],shipS,deltas,shipI)
    rocket = newObj(1000,1000,shipS,deltas,rocketI)
    table.insert(objects, ship)
    table.insert(objects, rocket)
end

function addRocks()
    deltas = {-speed,0}
    for i=0,numThings-1,2 do
        rock = newObj(right+(i*gap),randY(rockS[2]),rockS,deltas,rockI)
        table.insert(rocks, rock)
        table.insert(objects, rock)
    end
end

function addCoins()
    deltas = {-speed,0}
    for i=1,numThings,2 do
        coin = newObj(right+(i*gap)+(gap/3),randY(coinS[2]),coinS,deltas,coinI)
        table.insert(coins, coin)
        table.insert(objects, coin)
    end
end

function newObj(x, y, s, d, i)
    obj = {}
    obj.x = x
    obj.y = y
    obj.w = s[1]
    obj.h = s[2]
    obj.dx = d[1]
    obj.dy = d[2]
    obj.i = i
    return obj
end

function randY(height)
    return love.math.random(top, surface-height)
end

function love.update(dt)
    if started > 0 then
        gamePlay(dt)
    elseif triggerDown() then
        started = 1
    end
end

function playMusic()
    if not loop:isPlaying() then
        if loop == loop0 then
            if intro == false then
                loop = loop1
            else
                intro = false
            end
        elseif loop == loop1 then
            loop = loop2
        elseif loop == loop2 then
            loop = loop1
        end
        loop:play()
    end 
end

function stopMusic()
    if loop:isPlaying() then
        loop:stop()
        loop = loop0
        intro = true
    end
end

function moveObj(obj, dt)
    obj.x = obj.x + (obj.dx*dt)
    obj.y = obj.y + (obj.dy*dt)
end

function resetObj(obj)
    obj.x = obj.x + (gap*numThings)
    obj.y = randY(obj.h)
end

function shipFlying()
    if ship.dx < 0 then
        return false
    else
        return true
    end
end

function triggerDown()
    if love.mouse.isDown(1) then
        return true
    elseif love.keyboard.isDown("space") then
        return true
    else
        return false
    end
end

function gamePlay(dt)
    -- ship physics
    moveObj(ship, dt)
    if shipFlying() then
        ship.dy = ship.dy + (gravity*dt)
        if triggerDown() then
            if rocket.x > 999 then
                thrustSnd:play()
            end
            ship.dy = ship.dy - (thrust*dt)
            rocket.x = ship.x
            rocket.y = ship.y + ship.h
        else
            rocket.x = 1000
        end
        playMusic()
    elseif ship.x < -(width+gap) then
        gameStart()
    else
        ship.dx = ship.dx - 1
    end
    if ship.y+ship.h < top then
        ship.y = top-ship.h
        ship.dy = 0
    elseif ship.y > surface then
        gameOver()
    end
    -- rock physics
    for i,rock in ipairs(rocks) do
        moveObj(rock, dt)
        if shipFlying() then
            margin = 6
            if ship.x+ship.w > rock.x+margin and ship.x+margin < rock.x+rock.w and ship.y+ship.h > rock.y+margin and ship.y+margin < rock.y+rock.h then
                gameOver()
            elseif rock.x+rock.w < left then
                resetObj(rock)
            end
        else
            rock.dx = rock.dx - 1
        end
    end
    -- coin physics
    for i,coin in ipairs(coins) do
        moveObj(coin, dt)
        if shipFlying() then
            if ship.x+ship.w > coin.x and ship.x < coin.x+coin.w and ship.y+ship.h > coin.y and ship.y < coin.y+coin.h then
                coinSnd:play()
                score = score + 1
                resetObj(coin)
            elseif coin.x+coin.w < left then
                if score > 0 then
                    missSnd:play()
                    score = score - 1
                end
                resetObj(coin)
            end
        else
            coin.dx = coin.dx - 1
        end
    end
end

function gameOver()
    if ship.dx > -1 then
        deathSnd:play()
        if score > hiscore then
                hiscore = score
        end
        stopMusic()
        ship.dy = 0
        ship.dx = -speed
        rocket.x = 1000
    end
end

function love.draw()
    o = 0
    l = .33
    m = .66
    h = 1
    black =   {o,o,o}
    cyan =    {o,m,m}
    red =     {h,o,o}
    gray =    {h,h,h}
    brown =   {m,l,o}
    yellow =  {h,h,o}
    white =   {h,h,h}
    -- draw the visible field
    love.graphics.setColor(cyan)
    love.graphics.rectangle('fill', left, top, width, height)
    love.graphics.setColor(red)
    love.graphics.rectangle('fill', left, surface, width, bottom-surface)
    -- draw every object
    for i,obj in ipairs(objects) do
        intx = math.floor(obj.x)
        inty = math.floor(obj.y)
        love.graphics.setColor(white)
        --love.graphics.rectangle('line', obj.x, obj.y, obj.w, obj.h)
        love.graphics.draw(obj.i, intx, inty)
    end
    -- obscure the blank space around the field
    love.graphics.setColor(black)
    love.graphics.rectangle('fill', 0, 0, winwidth, top)
    love.graphics.rectangle('fill', 0, bottom, winwidth, winheight-top)
    love.graphics.rectangle('fill', 0, 0, left, winheight)
    love.graphics.rectangle('fill', right, 0, winwidth-right, winheight)
    -- draw score
    love.graphics.setColor(gray)
    love.graphics.print("score", left, top)
    love.graphics.print(score, left+50, top)
    love.graphics.print("hiscore", left+(width/2), top)
    love.graphics.print(hiscore, left+65+(width/2), top)
    -- print instructions
    love.graphics.setColor(cyan)
    wordH = height/5
    if started < 1 then
        love.graphics.print("press to start", right, top)
    end
    love.graphics.print("press for thrust", right, top+(wordH*1))
    love.graphics.setColor(yellow)
    love.graphics.print("collect coins", right, top+(wordH*2))
    love.graphics.setColor(brown)
    love.graphics.print("avoid rocks", right, top+(wordH*3))
    love.graphics.setColor(red)
    love.graphics.print("avoid bottom", right, top+(wordH*4))
end
